#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <ctype.h>

#define STR(s)  #s
#define XSTR(s) STR(s)

#define WORD_LENGTH_MAX 31
#define BUF_SIZE 1024

void split_words_of_file(FILE *file);
void clean_string(char *s);

int main(int argc, char **argv) {
  // Tester si il y a des arguments
  if (argc < 2) {
    fprintf(stderr, "Erreur pas assez d'arguments");
    exit(EXIT_FAILURE);
  }
  int arg_it = 1;
  FILE *fp;
  while (argv[arg_it] != NULL) {
    if (*argv[arg_it] == '-') {
      ++arg_it;
      fp = fopen(argv[arg_it], "r");
      if (fp == NULL) {
        perror("L'ouverture du fichier à échoué");
        exit(EXIT_FAILURE);
      }
      printf("Analyse du fichier : %s\n", argv[arg_it]);
      split_words_of_file(fp);
      printf("\nFin de l'Analyse du fichier\n");
      fclose(fp);
    } else {      // Si c'est un String
      clean_string(argv[arg_it]);
    }
    ++arg_it;
  }
  return EXIT_SUCCESS;
}

/**
 * Retourne l'ensemble des mots du fichier découpé mot par mot avec le caractére
 * \n pour indique une nouvelle ligne
 **/
void split_words_of_file(FILE *file) {
  char c;     // Variable pour fgetc
  int line = 1; // Ecrire les lignes
  char w[WORD_LENGTH_MAX + 1]; // Variable pour stocker le mot
  char *p = w; // Pointeur qui va remplir le mot
  printf("%d ", line);
  int cmpt = 0;
  while ((c = (char) fgetc(file))) {
    if (c == '\n') { //retour de ligne
      ++line; // Ligne de plus
      if (cmpt != 0) {
        // add hashtable(w, line)
        printf("Stock : %s \n", w);
        p = w;
        memset(p, '\0', (WORD_LENGTH_MAX + 1) * sizeof(char));
        cmpt = 0;
      }
      printf("%d ", line);
    } else if (c == EOF) {
      printf("Stock : %s", w);
      // add hashtable(w, line)
      break;
    } else if (c == ' ') { // lecture du mot terminé
      if (cmpt != 0) {
        printf("Stock : %s | ", w);
        // add hashtable(w, line)
        p = w;
        memset(p, '\0', (WORD_LENGTH_MAX + 1) * sizeof(char));
        cmpt = 0;
      }
    } else { //"mise en place du mot"
      if (!ispunct(c)) {
        *p = (char) c;
        ++p;
        ++cmpt;
      }
    }
  }
}

/**
 * Découpe la chaine de charactère en sous chaine de caractére les caractéres de
 * séparation sont "'/,;()"
 *
 */
void clean_string(char *s) {
  char delim[] = " '/,;:";
  char *ptr = strtok(s, delim);
  while (ptr != NULL) {
    printf("%s \n", ptr);
    // Add Hashtable ptr
    ptr = strtok(NULL, delim);
  }
}
