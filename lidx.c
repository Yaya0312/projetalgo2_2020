#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#define STR(s)  #s
#define XSTR(s) STR(s)

#define WORD_LENGTH_MAX 31
#define BUF_SIZE 1024

void split_words_of_file(FILE *file);
char *clean_string(char *s);

int main(int argc, char **argv) {
  // Tester si il y a des arguments
  if (argc < 2) {
    fprintf(stderr, "Erreur pas assez d'arguments");
    exit(EXIT_FAILURE);
  }
  int arg_it = 1;
  FILE *fp;
  while (argv[arg_it] != NULL) {
    if (*argv[arg_it] == '-') {
      ++arg_it;
      fp = fopen(argv[arg_it], "r");
      if (fp == NULL) {
        perror("L'ouverture du fichier à échoué");
        exit(EXIT_FAILURE);
      }
      printf("Analyse du fichier : %s\n", argv[arg_it]);
      split_words_of_file(fp);
      printf("\nFin de l'Analyse du fichier");
      fclose(fp);
    } else {      // Si c'est un String
      clean_string(argv[arg_it]);
    }
    ++arg_it;
  }
  return EXIT_SUCCESS;
}

/**
 * affiche chaque mot du fichier sur l'entré standard avec le numéro de la ligne
 * à chaque nouvelle ligne
 **/
void split_words_of_file(FILE *file) {
  char c;     // Variable pour fgetc
  int line = 1; // Ecrire les lignes
  char w[WORD_LENGTH_MAX + 1]; // Variable pour stocker le mot
  char *p = w; // Pointeur qui va remplir le mot
  printf("%d ", line);
  while ((c = (char) fgetc(file))) {
    if (c == '\n') { //retour de ligne
      // add hashtable(w, line)
      ++line; // Ligne de plus
      printf(" %s\n", w);
      printf("%d ", line);
      p = w;
      memset(p, '\0', (WORD_LENGTH_MAX + 1) * sizeof(char));
    } else if (c == ' ' || c == EOF) { // lecture du mot terminé
      printf("%s", w);
      // add hashtable(w, line)
      if (c == EOF) {
        break;
      }
      p = w;
      memset(p, '\0', (WORD_LENGTH_MAX + 1) * sizeof(char));
    } else { //"mise en place du mot"
      *p = (char) c;
      ++p;
    }
  }
}

/**
 * Découpe la chaine de caractére en sous chaine qui sont affichés sur l'entré
 * standard
 * */
char *clean_string(char *s) {
  char delim[] = " '/,;:";
  char *ptr = strtok(s, delim);
  while (ptr != NULL) {
    printf("'%s'\n", ptr);
    // Add Hashtable ptr
    ptr = strtok(NULL, delim);
  }
}
